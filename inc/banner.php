        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Pistola de pintura ar direto</h2>
                <p>Um dos principais processos para reparar peças galvanizadas é o processo de metalização. Com ele é possível revestir a peça com zinco a frio, o que é feito por meio de aspersão térmica.</p>
                <a href="<?=$url?>pistola-de-pintura-ar-direto" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Bico de solda oxigenio</h2>
                <p>Orce Bico de solda oxigenio, veja os melhores distribuidores, receba uma estimativa de preço agora com mais de 50 fábricas ao mesmo tempo gratuitamente para todo o Brasil</p>
                <a href="<?=$url?>bico-de-solda-oxigenio" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Pistola de pintura pulverizadora</h2>
                <p>Orce Pistola de pintura pulverizadora, descubra os melhores distribuidores, receba uma estimativa de preço agora com mais de 50 fábricas gratuitamente para todo o Brasil</p>
                <a href="<?=$url?>pistola-de-pintura-pulverizadora" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->