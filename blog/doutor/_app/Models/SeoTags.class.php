<?php

/**
 * SeoTags.class.php [ MODEL ]
 * Classe de apoio para o modelo LINK. Pode ser utilizada para gerar o SSEO para as páginas do sistema!
 * 
 * @copyright (c) 2016, Rafael da Silva Lima Web Stylus
 */
class SeoTags {

    private $File;
    private $Link;
    private $Data;
    private $Tags;

    /* Dados povoados */
    private $seoTags;
    private $seoData;

    /**
     * 
     * @param string $File = Arquivo da página
     * @param string $Link = Link da página
     */
    function __construct($File, $Link) {
        $this->File = strip_tags(trim($File));
        $this->Link = strip_tags(trim($Link));
    }

    /**
     * 
     * @return array = Retorna em um array as Tags, title, imagens, link etc..
     */
    public function getTags() {
        $this->checkData();
        return $this->seoTags;
    }

    /**
     * 
     * @return array = Retorna as informações do banco de dados.
     */
    public function getData() {
        $this->checkData();
        return $this->seoData;
    }

    ########################################
    ############ MÉTODOS PRIVATE ###########
    ########################################

    /**
     * Checa os dados para o método getData();
     */
    private function checkData() {
        if (!$this->seoData):
            $this->getSeo();
        endif;
    }

    /**
     * Método resposavel por definir o SEO de cada pagina dentro dos cases.
     */
    private function getSeo() {
        $ReadSeo = new Read;
        switch ($this->File):

            //SEO:: HOME - INDEX 
            case 'index';
                $this->Data = array('' . SITENAME, SITEDESC, BASE, INCLUDE_PATH . '/images/site.png');
                break;

            //SEO:: PERFIL 
            case 'profile';
                $this->Data = array('' . SITENAME, SITEDESC, BASE, INCLUDE_PATH . '/images/site.png');
                break;

            //SEO:: 404 DEFAULT
            default :
                $this->Data = array('', SITEDESC, BASE . '/404', INCLUDE_PATH . '/images/404.png');

        endswitch;

        if ($this->Data):
            $this->setTags();
        endif;
    }

    /**
     * Método que monta as tags do HEAD do site.
     * Pode ser melhorado com o tempo, manter atualizado de acordo com as novas normas da API
     */
    private function setTags() {
        $this->Tags['Title'] = $this->Data[0];
        $this->Tags['Content'] = Check::Words(html_entity_decode($this->Data[1]), 25);
        $this->Tags['Link'] = $this->Data[2];
        $this->Tags['Image'] = $this->Data[3];

        $this->Tags = array_map('strip_tags', $this->Tags);
        $this->Tags = array_map('trim', $this->Tags);

        $this->Data = null;

        //NORMAL PAGE
        //$this->seoTags = '<title>' . $this->Tags['Title'] . '</title> ' . "\n"; //Desativado por Robert Souza
		$this->seoTags = '<title>Doutores da Web</title> ' . "\n";
        $this->seoTags .= '<meta name="description" content="' . $this->Tags['Content'] . '"/>' . "\n";
        $this->seoTags .= '<meta name="robots" content="index, follow" />' . "\n";
        $this->seoTags .= '<link rel="canonical" href="' . $this->Tags['Link'] . '">' . "\n";
        $this->seoTags .= "\n";

        //FACEBOOK
        $this->seoTags .= '<meta property="og:site_name" content="' . SITENAME . '" />' . "\n";
        $this->seoTags .= '<meta property="og:locale" content="pt_BR" />' . "\n";
        //$this->seoTags .= '<meta property="og:title" content="' . $this->Tags['Title'] . '" />' . "\n"; //Desativado por Robert Souza
		$this->seoTags .= '<meta property="og:title" content="Doutores da Web" />' . "\n";
        $this->seoTags .= '<meta property="og:description" content="' . $this->Tags['Content'] . '" />' . "\n";
        $this->seoTags .= '<meta property="og:image" content="' . $this->Tags['Image'] . '" />' . "\n";
        $this->seoTags .= '<meta property="og:url" content="' . $this->Tags['Link'] . '" />' . "\n";
        $this->seoTags .= '<meta property="og:type" content="article" />' . "\n";
		//$this->seoTags .= '<meta property="fb:app_id" content="' . FB_APPID . '"/>' . "\n";
        //$this->seoTags .= '<meta property="fb:admins" content="' . FB_ADMINS . '"/>' . "\n";
        //ITEM GROUP (TWITTER)
        $this->seoTags .= '<meta itemgroup="name" content="' . $this->Tags['Title'] . '">' . "\n";
        $this->seoTags .= '<meta itemgroup="description" content="' . $this->Tags['Content'] . '">' . "\n";
        $this->seoTags .= '<meta itemgroup="url" content="' . $this->Tags['Link'] . '">' . "\n";

        $this->Tags = null;
    }

}
