

/* WhatsApp*/

.whatsapp{
    position: fixed;
    right: 15px;
    z-index: 9999;
    bottom: 80px;
   }
   .whats_msg {
    display: flex;
    align-items: center;
    background-color: rgb(255 255 255);
    border: 1px solid #ccc!important;
    border-radius: 11px;
    margin-right: 100px;
    margin: 10px 0;
    position: relative;
    box-shadow: 4px 3px 3px #4f46461c;
}

.whatsapp p.campo_vazio {
    text-align: center;
    font-size: 13px;
    font-weight: 600;
}

.whats_msg input{

    border: none;
    outline: none;
    border-radius: 0.65rem;
}
input#TelWhats {
    width: 100%;
    font-size: 18px;
    padding: 12px;
    text-align: center;
    font-weight: bold;
    background-color: rgb(255 255 255 / 0%);
    color: #464b4e;
}

input#TelWhats::placeholder{
    text-align: center;
    color: #979797;
}

   .modal-whatsapp{
       position: absolute;
       right: 70px;
       background-color: rgb(234 230 223);
       width: 300px;
       bottom: 20px;
       z-index: 10;
       text-align: center;
       display: none;
       border: transparent;
       border-radius: 17px;
       color:#474747;
       font-weight: bold;
       -webkit-box-shadow: 0px -1px 24px -8px rgba(133,133,133,1);
       -moz-box-shadow: 0px -1px 24px -8px rgba(133,133,133,1);
       box-shadow: 0px -1px 24px -8px rgba(133,133,133,1);
       animation: OpenWhats 0.5s ease-out;
   }

   .modal-whatsapp .closeModal{
    animation: closeModal 0.4s ease-out;
   }

   @keyframes closeModal {
    from {
      opacity: 1;
    }
    to {
      opacity: 0;
    }
  }

@keyframes OpenWhats {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}


   
   .modal-whatsapp::after{
     content: "";
     width: 0;
     height: 0;
     border-top: 15px solid transparent;
     border-bottom: 15px solid transparent;
     border-left: 15px solid  #eae6df;
     position: absolute;
     bottom: 1px;
     right: -7px;
     -webkit-box-shadow: 0px -1px 24px -8px rgba(133,133,133,1);
     -moz-box-shadow: 0px -1px 24px -8px rgba(133,133,133,1);
     box-shadow: 0px -1px 24px -8px rgba(133,133,133,1);
   }
   

   #modal_tremer{
    animation: tremer .2s;
    animation-iteration-count: 3;
  }


  @keyframes tremer {
    0% {right: 70px;}
    25% {right: 72px;}
    50% {right: 70px;}
    75% {right: 78px;}
    100% {right: 70px;}
}


   .modal-whatsapp ol{
       list-style-type: none;
       max-height: 350px;
       overflow-y: auto;
   }
   
   .modal-whatsapp ol a{
       text-decoration: none;
       color:#5a5a5a;
   }
   .modal-whatsapp .whatsapp-info{
     padding: 20px;
     border-top: #f4f4f4  solid 0.5px;
   }
   
   .modal-whatsapp .whatsapp-info span{
       display: flex;
       padding: 5px;
       align-items: center;
       justify-content: center;
   
     }
     .wp-loading{
       display: none;
     }
     .modal-whatsapp .whatsapp-header img {
        border: transparent;
        border-radius: 100px;
        width: 40px;
        margin-right: 15px;
        height: 40px;
        background-color: #fff;
        object-fit: cover;
    }

   
   
     .modal-whatsapp .whatsapp-header{
        background-color: rgb(0 128 105);
        border-radius: 17px 17px 0px 0px;
        padding: 10px;
        font-weight: 200;
        display: flex;
        align-items: center;
        justify-content: center;
   }
     .modal-whatsapp .whatsapp-header h3{
       color: #FFF;
       font-size: 0.8rem;
     }
     .modal-whatsapp .btn_modal_WhatsApp:hover {
        background-color: #103f24;
    }
     .modal-whatsapp .description{
          font-size: 1.2rem;
     }

     .modal-whatsapp p{
        margin: 0;
        padding: 0;
     }
   
     .modal-whatsapp .btn_modal_WhatsApp {
        padding: 12px 49px;
        border: transparent;
        border-radius: 10px;
        margin-top: 7px;
        background-color: #25d366;
        color: #fff;
        font-size: 15px;
        /* font-weight: bold; */
    }
     .modal-whatsapp form {
        padding: 17px 16px !important;
        display: flex;
        background-color: rgb(234 230 223);
        border-radius: 10px 10px;
        flex-direction: column;
        background-image: url();
    }
     .close_modal_Whats {
        position: absolute;
        right: 7px;
        top: 10px;
        cursor: pointer;
    }
    .btn-whatsapp span{
        position: absolute;
        background-color: rgb(221, 5, 5);
        padding: 5px;
        color: #fff;
        border: transparent;
        border-radius: 20px;
        top: -8px;
        font-size: 12px;
        right: 5px;

    }



     @media only screen and (max-width: 720px) {
   
       .whatsapp{
           position: fixed;
           right: 10px;
         
          }
   
          .modal-whatsapp{
             right: 20px;
             bottom: 60px;
          }
   
          .modal-whatsapp::after{
           content: "";
           width: 0;
           height: 0;
           border-top: 0px solid transparent;
           border-bottom: 0px solid transparent;
           border-left: 0px solid  transparent;
          }
     }


     @keyframes pulsate {
        0% { transform: scale(1); box-shadow: 0 0 10px rgba(238, 79, 79, 0.553); }
        50% { transform: scale(1.1); box-shadow: 0 0 20px rgba(239, 26, 26, 0.623); }
        100% { transform: scale(1); box-shadow: 0 0 10px rgba(239, 42, 42, 0.458); }
      }
  
      .btn-whatsapp span {
        animation: pulsate 1s infinite;
      }
   
     /* Fim*/